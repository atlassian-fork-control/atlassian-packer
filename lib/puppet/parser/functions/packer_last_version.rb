require 'net/http'

module Puppet::Parser::Functions
  newfunction(:packer_last_version, :type => :rvalue) do |args|
    uri = URI('https://checkpoint-api.hashicorp.com/v1/check/packer')
    metadata = Net::HTTP.get(uri)
    PSON::load(metadata)['current_version']
  end
end
