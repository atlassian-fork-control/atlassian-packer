# Install packer
class packer::install {
  ensure_packages(['unzip'])

  if $packer::version == 'latest' {
    $version = packer_last_version()
  } else {
    $version = $packer::version
  }

  $download_url = regsubst($packer::download_url, '\$\{version\}', $version, 'G')

  archive { '/tmp/packer.zip':
    ensure        => present,
    extract       => true,
    extract_path  => $packer::bin_dir,
    source        => $download_url,
    creates       => "$packer::bin_dir/packer",
    cleanup       => true,
  }
}
