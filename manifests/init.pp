# Manages and installs packer
class packer (
  String $version,
  Stdlib::Absolutepath $bin_dir,
  Stdlib::Httpurl $download_url = 'https://releases.hashicorp.com/packer/${version}/packer_${version}_linux_amd64.zip'
)  {
  class{'packer::install': }
  -> Class['packer']
}
